// --------- save user data in local storage --------
function saveData() {
    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;

    const user = {
        name: firstName + " " + lastName,
        email: email,
        password: password,
    }

    window.localStorage.setItem('user', JSON.stringify(user));
}


// ------- for validation user input ----------
function validate(event) {
    event.preventDefault();

    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let confirmPassword = document.getElementById("confirmPassword").value;
    let errorLabel = document.getElementsByClassName("error-label");
    let error = false;

    while (errorLabel.length > 0) {
        errorLabel[0].parentNode.removeChild(errorLabel[0]);
    }

    if (firstName.trim() == "" || !/^[a-zA-Z]+$/.test(firstName)) {
        error = true;
        let p = document.createElement('p');
        p.innerHTML = "First name should contain only alphabets";
        p.setAttribute("style", "color:red; margin-top:-10px; margin-bottom:10px");
        p.setAttribute("class", "error-label")
        document.getElementById("firstName").insertAdjacentElement("afterend", p)
    }

    if (lastName.trim() == "" || !/^[a-zA-Z]+$/.test(lastName)) {
        error = true;
        let p = document.createElement('p');
        p.innerHTML = "Last name should contain only alphabets";
        p.setAttribute("class", "error-label");
        p.setAttribute("style", "color:red; margin-top:-10px; margin-bottom:10px");
        document.getElementById("lastName").insertAdjacentElement("afterend", p);
    }

    if (email == "" || !/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email)) {
        error = true;
        let p = document.createElement('p');
        p.innerHTML = "Enter valid email";
        p.setAttribute("class", "error-label");
        p.setAttribute("style", "color:red; margin-top:-10px; margin-bottom:10px");
        document.getElementById("email").insertAdjacentElement("afterend", p);
    }

    if (password == "") {
        error = true;
        let p = document.createElement('p');
        p.innerHTML = "Enter password for your account";
        p.setAttribute("class", "error-label");
        p.setAttribute("style", "color:red; margin-top:-10px; margin-bottom:10px");
        document.getElementById("password").insertAdjacentElement("afterend", p);
    }

    if (confirmPassword == "") {
        let p = document.createElement('p');
        p.innerHTML = "Confirm your password";
        p.setAttribute("class", "error-label");
        p.setAttribute("style", "color:red; margin-top:-10px; margin-bottom:10px");
        document.getElementById("confirmPassword").insertAdjacentElement("afterend", p);

    } else if (password !== confirmPassword) {
        error = true;
        let p = document.createElement('p');
        p.innerHTML = "Password not matched";
        p.setAttribute("class", "error-label");
        p.setAttribute("style", "color:red; margin-top:-10px; margin-bottom:10px");
        document.getElementById("confirmPassword").insertAdjacentElement("afterend", p);
    }

    if (!document.getElementById('tnc').checked) {
        error = true;
        let p = document.createElement('p');
        p.innerHTML = "Accept the term and conditions";
        p.setAttribute("class", "error-label");
        p.setAttribute("style", "color:red; margin-top:-4px; padding-left:20px");
        document.querySelector(".term-and-condition").appendChild(p);
    }

    if (!error) {
        saveData();
        showHide();
    }
}

// ----- logout user 
function logoutUser() {
    window.localStorage.clear();
    window.location = "/";
}

// ----- for show and hide logout button in header and form container---- 
function showHide() {
    if (localStorage.getItem('user')) {
        document.querySelector('.sign-success').style.display = "block";
        document.querySelector('.logout').style.display = "block";
        document.querySelector('.container').style.display = "none";
        document.querySelector('.signup').style.display = "none";
    } else {
        document.querySelector('.logout').style.display = "none";
        document.querySelector('.container').style.display = "block";
        document.querySelector('.signup').style.display = "block";

    }
}


(() => {
    showHide();
})();
