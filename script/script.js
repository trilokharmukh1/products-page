const api = "https://fakestoreapi.com/products/";
const categoryApi = "https://fakestoreapi.com/products/categories";


// ---------  loader
function loader() {
    const loaderSection = document.querySelector('.loader');
    const emojis = ["🕐", "🕜", "🕑", "🕝", "🕒", "🕞", "🕓", "🕟", "🕔", "🕠", "🕕", "🕡", "🕖", "🕢", "🕗", "🕣", "🕘", "🕤", "🕙", "🕥", "🕚", "🕦", "🕛", "🕧"];
    const interval = 125;

    setInterval(() => {
        loaderSection.innerText = emojis[Math.floor(Math.random() * emojis.length)];
        // console.log(Math.floor(Math.random() * arr.length))
    }, interval);
}


// --------- show all product with category-----------
function allProduct(api) {
    fetch(api)
        .then((data) => {
            return data.json();
        })
        .then((products) => {
            document.querySelector(".preload").style.display = "none";
            document.querySelector(".offer").style.display = "block";


            if (products.length == 0) {
                let categoryDiv = document.createElement('div');
                categoryDiv.innerHTML = `<h1>No products available</h1>`;
                categoryDiv.setAttribute('class', 'category');
                document.getElementById('article-one').appendChild(categoryDiv)
            } else {

                products.forEach((product) => {
                    let category = product.category.replaceAll(' ', '-');
                    // console.log(document.getElementById(category));
                    if (!document.getElementById(category)) {

                        let categoryDiv = document.createElement('div');
                        categoryDiv.innerHTML = `<h1>${product.category.charAt(0).toUpperCase() + product.category.substring(1).toLowerCase()}</h1>
                                    <div id = ${category}-container class = "category-container"> </div>`;

                        categoryDiv.setAttribute('class', 'category');
                        categoryDiv.setAttribute('id', `${category}`);
                        document.getElementById('article-one').appendChild(categoryDiv)
                    }
                })

                products.forEach(product => {
                    let category = product.category.replaceAll(' ', '-');
                    // console.log(category);
                    if (document.getElementById(category)) {
                        let div = document.createElement('div');
                        div.innerHTML = `<img src="${product.image}" alt="product"><br />
                                                <div class="product-details">
                                                    <h3 class="product-title">${product.title}</h3><br />
                                                    <p class="product-description">${product.description}</p><br>
                                                    <p class="product-price">Price: <span>&dollar;${product.price}</span></p>
                                                    <div class="product-rating"> 
                                                        Rating: ${product.rating.rate} (${product.rating.count})
                                                    </div><br />
                                                    <button class="view-product" onClick="viewProduct(${product.id})">View Product</button><br>
                                                    
                                                </div>
                                            </div>`

                        // document.getElementById(`${category}-container`).innerHTML = "";
                        div.setAttribute('class', 'product-container');
                        document.getElementById(`${category}-container`).appendChild(div);

                    }
                });

            }
        })

        .catch((err) => {
            let categoryDiv = document.createElement('div');
            document.querySelector(".preload").style.display = "none";
            categoryDiv.innerHTML = `<h1 style="text-align:center; min-height:200px;">Oops!! something went wrong we will fix it soon.</h1>`;
            categoryDiv.setAttribute('class', 'category');
            document.getElementById('article-one').appendChild(categoryDiv)
            console.log(err);
        })
}

// -------- view single product --------------
function viewProduct(id) {
    document.querySelector(".preload").style.display = "flex";
    document.getElementById('article-one').innerHTML = ""

    fetch(`https://fakestoreapi.com/products/${id}`)
        .then((data) => {
            return data.json();
        })
        .then((product) => {
            console.log(product);
            document.querySelector(".preload").style.display = "none";

            let category = product.category.replaceAll(' ', '-');
            // console.log(document.getElementById(category));
            let categoryDiv = document.createElement('div');
            categoryDiv.innerHTML = `<div id = ${category}-container class = "category-container"> 
                <div class="product-container" style="width:600px;">
                    <div class="single-product" s>
                             <img src="${product.image}" alt="product" style="min-width:250px"><br />
                            <div class="product-details">
                                <h3 style="font-size:1.2em">${product.title}</h3><br />
                                <p class="view-product-description" > ${product.description}</p><br>
                                <p>Price: <span>&dollar;${product.price}</span></p>
                                <div> 
                                    Rating: ${product.rating.rate} (${product.rating.count})
                                </div><br />
                                <a href="./../index.html"><button> Back </button></a> 
                            </div>
                        </div>
                        </div>
                </div>
            
            </div>`;

            categoryDiv.setAttribute('class', 'category');
            categoryDiv.setAttribute('id', `${category}`);
            document.getElementById('article-one').appendChild(categoryDiv)
        })
        .catch((err) => {
            let categoryDiv = document.createElement('div');
            document.querySelector(".preload").style.display = "none";
            categoryDiv.innerHTML = `<h1 style="text-align:center; min-height:200px;">Oops!! something went wrong we will fix it soon.</h1>`;
            categoryDiv.setAttribute('class', 'category');
            document.getElementById('article-one').appendChild(categoryDiv);
            console.log(err);
        })
}


//  --------- initial calling function ---------
const init = () => {
    loader();
    allProduct(api);
    // createCategory(categoryApi);
    // showProduct();
}
init();




// --------- create category --------------
// function createCategory(categoryApi) {
//     fetch(categoryApi)
//         .then((res) => {
//             return res.json();
//         })
//         .then((categories) => {

//             categories.forEach((category) => {

//                 let option = document.createElement('option');
//                 option.innerHTML = `${category}`;

//                 option.setAttribute('value', `category/${category}`);

//                 document.getElementById('select-category').appendChild(option)

//             })
//         })
// }

// function showProduct() {
//     let selectValue = document.getElementById('select-category').value;
//     console.log("select product...");
//     console.log(selectValue);
//     allProduct(`https://fakestoreapi.com/products/${selectValue}`);

// }